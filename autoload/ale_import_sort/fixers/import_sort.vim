" Author: ronyclau (Ron Lau) <ron@ronlau.hk>
" Description: Integration of import-sort-cli with ALE. Requires patched
"              version of import-sort
"              (https://github.com/renke/import-sort/pull/125)

call ale#Set('javascript_import_sort_executable', 'import-sort')
call ale#Set('javascript_import_sort_use_global', get(g:, 'ale_use_global_executables', 0))
call ale#Set('javascript_import_sort_options', '')

function! ale_import_sort#fixers#import_sort#GetExecutable(buffer) abort
    return ale#node#FindExecutable(a:buffer, 'javascript_import_sort', [
    \   'node_modules/import-sort-cli/lib/index.js',
    \   'node_modules/.bin/import-sort',
    \])
endfunction

function! ale_import_sort#fixers#import_sort#Fix(buffer) abort
    let l:options = ale#Var(a:buffer, 'javascript_import_sort_options')

    return {
    \   'command': ale#Escape(ale_import_sort#fixers#import_sort#GetExecutable(a:buffer))
    \       . ' --stdin'
    \       . ' --stdin-filepath=%s'
    \       . ' ' . l:options,
    \}
endfunction
