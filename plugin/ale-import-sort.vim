if exists("g:ale_import_sort_loaded")
    finish
endif

let g:ale_import_sort_loaded = 1

call ale#fix#registry#Add(
            \ "import_sort",
            \ "ale_import_sort#fixers#import_sort#Fix",
            \ ['javascript', 'typescript'],
            \ "Sort import of JavaScript and TypeScript files."
            \)
