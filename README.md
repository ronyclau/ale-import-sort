# ale-import-sort

ALE plugin for invoking `import-sort`.

Requires a patched version of `import-sort` (https://github.com/renke/import-sort/pull/125).
